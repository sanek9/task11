#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Queue{
	void* value;
	struct Queue* next;
	struct Queue* first;
};

struct Queue* newQueue(){
	struct Queue* n = (struct Queue*) malloc(sizeof(struct Queue));
	n->first=n;
	n->next=NULL;
	n->value=NULL;
	return n;
}
struct Queue* addQ(struct Queue* q, void* value){
/*	if (!q||!q->value){
		q = (q)?q:newQueue();
		q->value = value;
		return q;
	};*/
	if (!q){
		q = newQueue();
		q->value = value;
		return q;
	};
	q->next = newQueue();
	q->next->value = value;
	q->next->first = q->first;
	return q->next;
}

void delQueue(struct Queue* q){
	q=q->first;
	struct Queue* ql=q;
	while(ql = q->next){
		free(q);
		q=ql;
	}
}
struct Map{
    char* key;
    void* value;
    struct Map* next;
    struct Map* priv;
};
typedef struct{
	char name[256];
	struct Map* subdirs;
	struct Map* files;
}Directory;
typedef struct{
	char name[256];
}File;
struct Map* newMap(){
	struct Map* n = (struct Map*) malloc(sizeof(struct Map));
	n->next=NULL;
	n->priv=NULL;
	n->key=NULL;
	n->value=NULL;
	return n;
}

struct Map* removeM(struct Map* m){
	if(m==NULL)return NULL;
	if(m==m->priv) {
		m->priv=NULL;
	}
	if(m==m->next){
		m->next=NULL;
	}

	struct Map* s = m->priv;
	if(m->next)
		m->next->priv = m->priv;
	if(m->priv)
		m->priv->next = m->next;
	if(m->key)
		free(m->key);
	free(m);
	return s;
}
struct Map* findM(struct Map* m, char* key){
	int res;
	while(m)
		if((res=strcmp(key, m->key))>0){
			m=m->next;
		}else if(res<0){
			m=m->priv;
		}else{
			//printf("found key = %s\n", key);
			return m;
		}
	//printf("not found key = %s\n", key);
	return NULL;
}
struct Map* insertM(struct Map* m, char* key, void* value){
    struct Map* n = m;
    struct Map* z;
    int res;
    z = newMap();
	z->key = key;
	z->value = value;
    if(!m){
		return z;
	}
	while(m)
		if((res = strcmp(key, m->key))>0){
			if(!m->next){
				m->next=z;
				return n;
			}
			m=m->next;//
		}else if(res<0){
			if(!m->priv){
				m->priv=z;
				return n;
			}
			m=m->priv;
		}else{
			m->value = value;
			free(z);
			return n;
		}

}
void INFIX_TRAVERSE(struct Map* m, void (*func)(void*, int), int sp){
	if(!m)
		return;
	if(m->priv)
		INFIX_TRAVERSE(m->priv, func, sp);
	func(m->value, sp);
	if(m->next)
		INFIX_TRAVERSE(m->next, func, sp);
}
struct Queue* addSEl(struct Queue* q, void* value, int (*compare_func)(void*, void*)){
	struct Queue* m = NULL;
	if(q){
		while(q->next&&(compare_func(value, q->next->value)>0)){
			q=q->next;
		}
		m=q->next;
	}
	q = addQ(q, value);
	q->next=m;
	return q;
}
void correctLinks(struct Queue* first){
	struct Queue* q = first;
	while(q){
		q->first = first;
		q=q->next;
	}
}
void addFile(Directory* dir, char* name){
	if(!dir)
		exit(-2);
	File* file = (File*) malloc(sizeof(File));
	strcpy(file->name, name);
	//printf("%d ", dir->files);
	dir->files = insertM(dir->files,file->name, file);
	//printf("%d\n", dir->files);
}
Directory* addDir(Directory* dir, char* name){
	Directory* newDir = (Directory*) malloc(sizeof(Directory));
	strcpy(newDir->name, name);
	newDir->subdirs=NULL;
	newDir->files=NULL;
	if(dir!=NULL)
		dir->subdirs = insertM(dir->subdirs,newDir->name, newDir);

	return newDir;
}
void printFiles(struct Queue* f){

	while(f){
		printf("\t%s\n",((File*)f->value)->name );
		f=f->next;
	}
}
int compare(void * a, void* b){
	//return *(((int*)a))-*(((int*)b));
//	printf("strc %s\n", ((File*)a)->name);
	return strlen(((File*)a)->name)-strlen(((File*)b)->name);
}
void sortFiles(struct Map* files, const struct Queue* first){
	if(!files)
		return;

	sortFiles(files->priv, first);
//	printf("\t\t\t %s\n",((File*)files->value)->name );
	//printf("\t\tto %d\n",(files->value) );
	addSEl( first, files->value, compare);
	sortFiles(files->next, first);
}
void sortedPrintFiles(struct Map* files){
	struct Queue* f;
	f = newQueue();
	sortFiles(files, f);
	correctLinks(f->next);
	printFiles(f->next);
	delQueue(f);
	printf("\n");
}

void printDir(struct Map* subdirs, struct Queue* q){
	if(subdirs==NULL)
		return;
	Directory* d = (Directory*) subdirs->value;

	struct Queue* qq;
	printDir(subdirs->priv, q);

	if(d->files){
		if(q){
			qq=q->first;
			while(qq){
				printf("/%s", ((Directory*) qq->value)->name);
				qq=qq->next;
			}
		}
		printf("/%s/\n",d->name);
//	if(d->files){
		sortedPrintFiles(d->files);
	}
	qq = addQ(q, d);
	printDir(d->subdirs, qq);
	free(qq);
	if(q)
		q->next=NULL;

	printDir(subdirs->next, q);
	//q = addQ(q, d);
	//free(q);
	//INFIX_TRAVERSE(dir->subdirs, printDir,sp+1);



}

int newFile( Directory* dir, struct Queue* path){
	if(!dir)
		exit(-5);
	Directory* d = NULL;
	struct Map* m;
	if(path->next){
		if(dir->subdirs){
			m = findM(dir->subdirs, (char*) path->value);
			d = m? m->value:NULL;
		}
		if(!d){
			d = addDir(dir, (char*) path->value);
		}
		newFile(d, path->next);
	} else {

		if(dir->files||!dir->files&&!findM(dir->files, (char*) path->value)){
			addFile(dir, (char*) path->value);
		}else
			//addFile(dir, (char*) path->value);
			return -2;
	}
}
struct Queue* getPath(char* str){
	char separators[] = {'/','\\'};
	struct Queue* path = NULL;
	str=(*str=='/'||*str=='\\')?str+1:str;
	path = addQ(path, str);
	while(str=strpbrk(str, separators)){
		*str='\0';
		path = addQ(path, ++str);
	}
	return path->first;
}
int main()
{
	Directory* root = addDir(NULL, "root");
/*	addDir(root, "z123");
	Directory* h;
	h = addDir(addDir(root, "k"), "34");
	addFile(h, "newfile");
	addFile(h, "uiy");
	addFile(h, "rfwe");
	addFile(h, "4554");
	h = addDir(addDir(root, "hello"), "fdg");
	addFile(h, "saddsg");*/
/*	int arr[]={1,5,8,9,6,3};
	struct Queue* q = newQueue();
	addSEl(q, &arr[0], compare);
	addSEl(q, &arr[1], compare);
	addSEl(q, &arr[2], compare);
	addSEl(q, &arr[3], compare);
	addSEl(q, &arr[4], compare);
	addSEl(q, &arr[5], compare);*/

	int N = 15;
	int i;
	char paths[16][200] =
		{"/sdf/dsfsw/tnfg/qweqw",
		 "asjk/ger/asfgr/adsd",
		 "fsdf/erew/dfgdfg/sdq",
		 "dsfg/rtww/hehrgw/dsgfsd",
		 "sdfsdf/sdfsddf/sdfsd",
		 "sdfsdf/sdfsddf/gsdd",
		 "sdfsdf/wada",
		 "sdfsdf/asda",
		 "fsdf/erew/dfgdfg/sdqsdf",
		 "sdf/dsfsw/tnfg/qweqsdw",
		 "d/kjbkl",
		 "d/kjh",
		 "d/kjhnkjn",
		 "d/jhnk",
		 "d/lkj"};
	struct Queue* q;
/*
	q->value = "12";
	q= addQ(q, "dsf");
	q=addQ(q, "43");
	q=q->first;
	delQueue(q);*/
/*	while(q){
		printf("%d\n",*((int*)q->value));
		q=q->next;
	}
//	q=q->first;*/
	char buf[500];
	while(1){
		//fflush(stdin);
		__fpurge(stdin);
		fgets(buf, sizeof(buf), stdin);
		if(buf[0]=='\n')
			break;
		buf[strlen(buf)-1]='\0';
		if(!(q=getPath(buf))){
			printf("Wrong input\n");
			continue;
		}
		newFile(root, q);

	}
/*	for(i = 0;i<N; i++){
		//printf("%s\n", paths[i]);
		newFile(root, q = getPath(paths[i]));
		delQueue(q);
	}*/
	if(root->files){
		printf("/\n");
		sortedPrintFiles(root->files);
	}
	printDir(root->subdirs, NULL);

    printf("Hello world!\n");
    return 0;
}
